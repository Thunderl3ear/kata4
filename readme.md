# Kata 4 - A palindrome checker
Takes a word or a sentence as an input. Removes spaces and converts the string to a vector of lower case chars. Each char is then checked for being in the range 'a' to 'z'. A reversed version of the vector is created and compared elementwise with the original. Displays the result in the console.


## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
As a minimum:
* Requires `gcc`.

## Usage
* mkdir build
* Compile with: g++ main.cpp -o build/main
* Run: ./build/main

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
