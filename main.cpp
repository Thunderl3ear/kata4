// Author: Thorbjoern
// Check if a word or sentence is a palindrome

#include<iostream>
#include<string>
#include<vector>
#include<algorithm>

std::vector<char> convert_to_vector(std::string input)
{
    std::vector<char> vectorized;
    for (size_t i = 0; i < input.length(); i++)
    {
        if(input[i] != ' ')
        {
            vectorized.push_back(std::tolower(input[i]));
        }
    }
    return vectorized;
}

bool is_palindrome(std::vector<char> palindrome)
{
    std::vector<char> reversed = palindrome;
    std::reverse(reversed.begin(), reversed.end());
    if(reversed == palindrome)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void show_vector(std::vector<char> word)
{
    for(char letter : word)
    {
        std::cout<<letter;
    }
    return;
}

bool check_valid_word(std::vector<char> word)
{
    for(char letter : word)
    {
        int letter_val = letter;
        if((letter_val<97) || (letter_val>122))
        {
            return false;
        }
    }
    return true;
}

int main()
{
    std::cout<<"Write a word/sentence"<<std::endl;
    std::string input;
    getline(std::cin, input);

    std::vector<char> palindrome = convert_to_vector(input);
    if(check_valid_word(palindrome))
    {
        if(is_palindrome(palindrome))
        {
            std::cout<<"The word ";
            show_vector(palindrome);
            std::cout<<" is a palindrome"<<std::endl;
        }
        else
        {
            std::cout<<"The word ";
            show_vector(palindrome);
            std::cout<<" is not a palindrome"<<std::endl;
        }
    }
    else
    {
        std::cout<<"Illegal characters used. Terminating"<<std::endl;
    }
    return 0;
}